(TeX-add-style-hook
 "7-Insurance-Moral-Hazard-slides"
 (lambda ()
   (LaTeX-add-bibitems
    "BlackwellGirshick1954TGSD"
    "Carmichael1983JOLE"
    "Green-Stokey1983JPE"
    "Holmstrom1979BJE"
    "HolmstromMilgrom1987Em"
    "HolmstromMilgrom1991JLEO"
    "Lazear-Rosen1981JPE"
    "Malcomson1984JPE"
    "Mookherjee1984RES"))
 :latex)

