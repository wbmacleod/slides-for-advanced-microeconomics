#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
%Choose a themer (uncomment desired)
%\usetheme{Berkeley}
%\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montepellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}
%\usecolortheme{albatross}
\usecolortheme{beaver}
%\usecolortheme{beetle}
%\usecolortheme{crane}
%\usecolortheme{fly}
\setbeamertemplate{footline}[page number] 
\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\usepackage{pgf}% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
%Comment out if Navigation symbols wanted
\setbeamertemplate{navigation symbols}{}
%% Uncomment relevant options
%\setbeameroption{show notes}
\setbeameroption{hide notes}
%\setbeameroption{show only notes}
\end_preamble
\options show only notes
\use_default_options false
\begin_modules
theorems-ams
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding utf8
\fontencoding global
\font_roman "times" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
This file is a solution template for:
\end_layout

\begin_layout Itemize
Talk at a conference/colloquium.
 
\end_layout

\begin_layout Itemize
Talk length is about 20min.
 
\end_layout

\begin_layout Itemize
Style is ornate.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
 
\end_layout

\begin_layout Plain Layout
In principle, this file can be redistributed and/or modified under the terms
 of the GNU Public License, version 2.
 However, this file is supposed to be a template to be modified for your
 own needs.
 For this reason, if you use this file as a template and not specifically
 distribute it as part of a another package/program, the author grants the
 extra permission to freely copy and modify this file as you see fit and
 even to delete this copyright notice.
 
\end_layout

\end_inset


\end_layout

\begin_layout Title
Chapter 8 - Trade with Asymmetric Information - 2 - Welfare Analysis 
\end_layout

\begin_layout Author
Advanced Microeconomics
\end_layout

\begin_layout Date
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
today
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
The following causes the table of contents to be shown at the beginning
 of every subsection.
 Delete this, if you do not want it.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%
\backslash
AtBeginSubsection[]{%
\end_layout

\begin_layout Plain Layout

%  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

%    
\backslash
frametitle{Outline}   
\end_layout

\begin_layout Plain Layout

%    
\backslash
tableofcontents[currentsection,currentsubsection] 
\end_layout

\begin_layout Plain Layout

%  }
\end_layout

\begin_layout Plain Layout

%}
\end_layout

\begin_layout Plain Layout

% Set notes options in preamble.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
If you wish to uncover everything in a step-wise fashion, uncomment the
 following command:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSection[]{%
\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

    
\backslash
frametitle{Outline}   
\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection] 
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Outline
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
beamerdefaultoverlayspecification{<+->}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Introduction
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
These slides discuss the notion of an efficient allocation at three stages:
\end_layout

\begin_deeper
\begin_layout Itemize

\emph on
Ex ante: 
\emph default
allocations agreed upon before parties learn their private information
\end_layout

\begin_layout Itemize

\emph on
Interim: 
\emph default
allocations agreed upon when parties learn their type, but not the type
 of the other party.
\end_layout

\begin_layout Itemize
Ex 
\emph on
post:
\emph default
 allocations agreed upon after all private information is revealed.
\end_layout

\end_deeper
\begin_layout Itemize
What are the properties of an allocation that satisfies the incentive constraint
s?
\end_layout

\begin_layout Itemize
What are the properties of an 
\emph on
ex post 
\emph default
efficient allocation that satisfies the incentive constraints?
\end_layout

\end_deeper
\begin_layout Section
Normative Evaluation of Allocations with Asymmetric Information
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Setup
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
In this section we consider the welfare evaluation of allocations when there
 is asymmetric information.
\end_layout

\begin_layout Itemize
Consider the buyer-seller model with allocations given by:
\begin_inset Formula 
\[
\vec{x}\in\Xi=\left\{ \vec{x}\left(\cdot\right)|x:\Omega\rightarrow A\right\} .
\]

\end_inset


\end_layout

\begin_layout Itemize
The subset of such mechanisms that are 
\emph on
incentive compatible
\emph default
 is denoted by the set 
\begin_inset Formula $\Xi^{IC}\subset\Xi$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Incentive Compatible Allocations
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Let:
\begin_inset Formula 
\begin{align}
u^{B}\left(\vec{x}|\hat{b},b\right)\equiv & \int_{c_{L}}^{c_{H}}u^{B}\left(\vec{x}\left(\hat{b},c\right),b\right)dG\left(c\right),\label{8-int_B}\\
u^{S}\left(\vec{x}|\hat{c},c\right)\equiv & \int_{b_{L}}^{b_{H}}u^{S}\left(\vec{x}\left(b,\hat{c}\right),c\right)dF\left(b\right).\label{8-int_S}
\end{align}

\end_inset


\end_layout

\begin_layout Itemize
An allocation satisfies 
\begin_inset Formula $\vec{x}\in\Xi^{IC}$
\end_inset

 if: 
\begin_inset Formula 
\begin{eqnarray*}
u^{B}\left(\vec{x}|b\right)\equiv u^{B}\left(\vec{x}|b,b\right) & \geq & u^{B}\left(\vec{x}|\hat{b},b\right),\\
u^{S}\left(\vec{x}|c\right)\equiv u^{S}\left(\vec{x}|c,c\right) & \geq & u^{S}\left(\vec{x}|\hat{c},c\right),
\end{eqnarray*}

\end_inset

for all 
\begin_inset Formula $\hat{b},b\in\left[b_{L},b_{H}\right]$
\end_inset

 and 
\begin_inset Formula $\hat{c},c\in\left[c_{L},c_{H}\right].$
\end_inset

 
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ex Ante Efficient Allocations
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\emph on
Ex ante 
\emph default
allocations are evaluated before parties observe their private information:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{eqnarray*}
u_{A}^{B}\left(\vec{x}\right) & = & E\left\{ u^{B}\left(\vec{x}|b\right)\right\} ,\\
u_{A}^{S}\left(\vec{x}\right) & = & E\left\{ u^{S}\left(\vec{x}|c\right)\right\} .
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Definition*
An allocation 
\begin_inset Formula $\vec{x}\in\Xi\ (\vec{x}\in\Xi^{IC})$
\end_inset

 is 
\emph on
ex ante
\emph default
 efficient (respectively 
\emph on
ex ante
\emph default
 incentive efficient) if there does not exist 
\begin_inset Formula $\vec{x}'\in\Xi\ (\vec{x}'\in\Xi^{IC})$
\end_inset

 such that 
\begin_inset Formula $u^{B}\left(\vec{x}^{\prime}\right)\geq u^{B}\left(\vec{x}\right)$
\end_inset

 and 
\begin_inset Formula $u^{S}\left(\vec{x}^{\prime}\right)\geq u^{S}\left(\vec{x}\right)$
\end_inset

 where at least one inequality is strict.
 
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ex Ante Efficient Allocations
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Proposition*
Suppose courts enforce specific performance.
 Then every 
\emph on
ex ante efficient
\emph default
 allocation is payoff equivalent to some 
\emph on
ex ante incentive efficient
\emph default
 allocation with a price function given by: 
\begin_inset Formula 
\[
p^{\ast}\left(\hat{b},\hat{c}\right)=\int_{b_{L}}^{b_{H}}B\left(q^{\ast}\left(b,\hat{c}\right),b\right)dF\left(b\right)+\int_{c_{L}}^{c_{H}}C\left(q^{*}\left(\hat{b},c\right),c\right)dG\left(c\right)+\bar{p},
\]

\end_inset

where 
\begin_inset Formula $q^{\ast}\left(b,c\right)$
\end_inset

 is the 
\emph on
ex ante
\emph default
 efficient quantity as a function of individual types.
 
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Sketch of Proof
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
L
\size scriptsize
et 
\begin_inset Formula $q^{*}\left(b,c\right)$
\end_inset

 be the 
\emph on
ex post
\emph default
 efficient quantity, and suppose the price function is given by 
\begin_inset Formula $p^{*}$
\end_inset

.
\end_layout

\begin_layout Itemize

\size scriptsize
Consider a the seller's problem:
\begin_inset Formula 
\begin{align*}
\int_{b_{L}}^{b_{H}}\left(p^{\ast}\left(\tilde{b},c\right)-C\left(q^{*}\left(\tilde{b},c\right),c\right)\right)dF\left(\tilde{b}\right) & =\\
\int_{b_{L}}^{b_{H}}\left(B\left(q^{\ast}\left(\tilde{b},c\right),\tilde{b}\right)+\int_{c_{L}}^{c_{H}}C\left(q^{*}\left(\tilde{b},\tilde{c}\right),\tilde{c}\right)dG\left(\tilde{c}\right)-C\left(q^{*}\left(\tilde{b},c\right),c\right)\right)dF\left(\tilde{b}\right)+\bar{p} & =\\
\int_{b_{L}}^{b_{H}}\left(B\left(q^{\ast}\left(\tilde{b},c\right),\tilde{b}\right)-C\left(q^{*}\left(\tilde{b},c\right),c\right)\right)dF\left(\tilde{b}\right)+K & \geq\\
\int_{b_{L}}^{b_{H}}\left(B\left(q^{\ast}\left(\tilde{b},\hat{c}\right),\tilde{b}\right)-C\left(q^{*}\left(\tilde{b},\hat{c}\right),\hat{c}\right)\right)dF\left(\tilde{b}\right)+K
\end{align*}

\end_inset

The constant is given by: 
\begin_inset Formula $K=\int_{b_{L}}^{b_{H}}\int_{c_{L}}^{c_{H}}C\left(q^{*}\left(\tilde{b},\tilde{c}\right),\tilde{c}\right)dG\left(\tilde{c}\right)dF\left(\tilde{b}\right)+\bar{p}$
\end_inset

.
 
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Sketch of Proof - 2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
The third line is the total surplus plus a constant, and hence 
\begin_inset Formula $q^{*}\left(b,c\right)$
\end_inset

 maximizes this expression, which in turn implies that one cannot gain by
 being untruthful.
\end_layout

\begin_layout Itemize
At the 
\emph on
ex ante
\emph default
 stage, parties care only about the expected payment, which can be set to
 any level by the appropriate choice of 
\begin_inset Formula $\bar{p}$
\end_inset

.
 Hence, any 
\emph on
ex ante
\emph default
 efficient payoffs can be achieved with an incentive compatible allocation.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Role of Specific Performance
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
This is a very general result based upon a simple idea:
\end_layout

\begin_deeper
\begin_layout Itemize
Ensure each party faces the full cost and benefit from trade.
\end_layout

\begin_layout Itemize
Follows from the work of 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
citet{Clarke1971PC}
\end_layout

\end_inset

, 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
citet{Groves1973Em}
\end_layout

\end_inset

, 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
citet{DAspremontGerard-Varet1979JPubE}
\end_layout

\end_inset

.
\end_layout

\end_deeper
\begin_layout Itemize
However, this implies that there can be great variance in payments at the
 interim period, and hence in general these allocations are not necessarily
 
\emph on
interim individually rational.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Interim Efficient Allocations
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Suppose parties bargain at the 
\emph on
interim stage, 
\emph default
after they have learned their types, but before they learn the type of the
 other party.
\end_layout

\begin_layout Definition*
An allocation 
\begin_inset Formula $\vec{x}\in\Xi~(\vec{x}\in\Xi^{IC})$
\end_inset

 is 
\emph on
interim
\emph default
 efficient (respectively 
\emph on
interim
\emph default
 incentive efficient) if there does not exist 
\begin_inset Formula $\vec{x}'\in\Xi~(\vec{x}'\in\Xi^{IC})$
\end_inset

 such that 
\begin_inset Formula $u^{B}\left(\vec{x}^{\prime}|b\right)\geq u^{B}\left(\vec{x}|b\right)$
\end_inset

 and 
\begin_inset Formula $u^{S}\left(\vec{x}^{\prime}|c\right)\geq u^{S}\left(\vec{x}|c\right)$
\end_inset

 for every 
\begin_inset Formula $\left\{ b,c\right\} \in\Omega$
\end_inset

, with at least one inequality that is strict.
\end_layout

\begin_layout Itemize
The set of 
\emph on
interim efficient allocations
\emph default
 and 
\emph on
interim incentive efficient allocations
\emph default
 are denoted by 
\begin_inset Formula $\Xi_{I}\subset\Xi$
\end_inset

 and 
\begin_inset Formula $\Xi_{I}^{IC}\subset\Xi^{IC}$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
citet{HolmstromMyerson1983Em}
\end_layout

\end_inset

 (theorem 1) and 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
citet{Kobayashi1980E}
\end_layout

\end_inset

 show that an incentive-compatible decision rule is interim incentive efficient
 if and only if there does not exist any common-knowledge event such that
 it is interim dominated given this information by another incentive-compatible
 decision rule.
 
\end_layout

\begin_layout NoteItem
Notice that if the gain from trade is not common knowledge, they parties
 cannot agree to change allocation.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ex Post Efficient Allocations
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Definition*
An allocation 
\begin_inset Formula $\vec{x}\in\Xi~(\vec{x}\in\Xi^{IC})$
\end_inset

 is 
\emph on
ex post
\emph default
 efficient (respectively 
\emph on
ex post
\emph default
 incentive efficient) if there does not exist 
\begin_inset Formula $\vec{x}'\in\Xi~(\vec{x}'\in\Xi^{IC})$
\end_inset

 such that 
\begin_inset Formula $u^{B}\left(\vec{x}^{\prime}|b,c\right)\geq u^{B}\left(\vec{x}|b,c\right)$
\end_inset

 and 
\begin_inset Formula $u^{S}\left(\vec{x}^{\prime}|b,c\right)\geq u^{S}\left(\vec{x}|b,c\right)$
\end_inset

 for every 
\begin_inset Formula $\left(b,c\right)\in\Omega$
\end_inset

, where at least one inequality is strict.
 
\end_layout

\begin_layout Itemize
The set of 
\emph on
ex post
\emph default
 efficient allocations and 
\emph on
ex post
\emph default
 incentive efficient allocations are denoted by 
\begin_inset Formula $\Xi_{P}\subset\Xi$
\end_inset

 and 
\begin_inset Formula $\Xi_{P}^{IC}\subset\Xi^{IC}$
\end_inset

 respectively.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Welfare Theorem with Asymmetric Information
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Proposition*
Let 
\begin_inset Formula $\Xi_{A},\Xi_{I},\,\Xi_{P}$
\end_inset

 and 
\begin_inset Formula $\Xi_{A}^{IC},\Xi_{I}^{IC},\,\Xi_{P}^{IC}$
\end_inset

 denote the set of ex ante, interim, ex post efficient and ex ante, interim,
 ex post incentive efficient allocations.
 If allocations are constrained to be continuous then: 
\begin_inset Formula 
\begin{equation}
\Xi_{A}\subset\Xi_{I}\subset\Xi_{P}\,and\,\Xi_{A}^{IC}\subset\Xi_{I}^{IC}\subset\Xi_{P}^{IC},\label{8-eq:Xi-inclusions}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\Xi_{A}\cap\Xi^{IC}\subset\Xi_{A}^{IC},\label{8-eq:A-IC}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\Xi_{I}\cap\Xi^{IC}\subset\Xi_{I}^{IC},\label{8-eq:I-IC}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\Xi_{P}\cap\Xi^{IC}\subset\Xi_{P}^{IC}.\label{8-eq:P-IC-1}
\end{equation}

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Proof
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
If an allocation 
\begin_inset Formula $\vec{x}$
\end_inset

 is 
\emph on
ex ante
\emph default
 efficient, but not 
\emph on
interim
\emph default
 efficient, then there is an allocation 
\begin_inset Formula $\vec{x}'$
\end_inset

 that is strictly better than 
\begin_inset Formula $\vec{x}$
\end_inset

 for some states either for the buyer or for the seller.
 Suppose it is the buyer, then the continuity of the allocation implies:
\begin_inset Formula 
\begin{eqnarray*}
E\left\{ u^{B}\left(\vec{x}^{\prime}|b\right)\right\}  & > & E\left\{ u^{B}\left(\vec{x}|b\right)\right\} ,\\
E\left\{ u^{S}\left(\vec{x}^{\prime}|c\right)\right\}  & \geq & E\left\{ u^{S}\left(\vec{x}|c\right)\right\} ,
\end{eqnarray*}

\end_inset

and hence 
\begin_inset Formula $\vec{x}$
\end_inset

 is not 
\emph on
ex ante
\emph default
 efficient.
 
\end_layout

\begin_layout Itemize
A similar argument applies for the other cases.
\end_layout

\begin_layout NoteItem
If parties reach an 
\emph on
ex ante
\emph default
 incentive efficient agreement, then the agreement remains incentive efficient
 at the 
\emph on
interim
\emph default
 and 
\emph on
ex post
\emph default
 stages.
 Thus, parties can choose allocations at the 
\emph on
ex ante
\emph default
 stage that are also both 
\emph on
interim
\emph default
 and 
\emph on
ex post
\emph default
 efficient.
 Thus, by itself, asymmetric information does not necessarily imply inefficient
 exchange, nor conflict.
 A necessary condition for this result is that specific performance is an
 enforceable legal rule that binds parties to each other at both the 
\emph on
interim
\emph default
 and 
\emph on
ex post
\emph default
 stages.
 In general, most relationships provide conditions under which parties may
 leave.
 
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
beamerdefaultoverlayspecification{}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Bibliography
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
btprint "btPrintCited"
bibfiles "/home/wbmacleod/Briefcase/Bibtex/refer"
options "chicagoa"

\end_inset


\end_layout

\end_body
\end_document
